#include "funky_bool.h"

#include <sstream>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(FunkyBoolThing, OutputTrue) {
  std::ostringstream oss;
  oss << FunkyBool(true);
  EXPECT_EQ(oss.str(), "T");
}

TEST(FunkyBoolThing, OutputFalse) {
  std::ostringstream oss;
  oss << FunkyBool(false);
  EXPECT_EQ(oss.str(), "F");
}

#pragma once

#include <iostream>
#include <string>

class FunkyBool {
  public:
    FunkyBool() : val_(false) {}
    FunkyBool(bool val) : val_(val) {}
    operator bool() const { return val_; }
  private:
    bool val_;
};

std::ostream& operator<<(std::ostream& out, const FunkyBool& fb) {
  return out << (fb ? 'T' : 'F');
}


#include "geometry.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(Geometry, ZeroVector) {
  EXPECT_EQ(Vec3(), Vec3(0, 0, 0));
  EXPECT_EQ(Vec3(), 0 * Vec3(1, 2, 3));
  EXPECT_EQ(length(Vec3()), 0);
}

TEST(Geometry, Scalars) {
  Vec3 a(1, 2, 4);
  Vec3 b(4, 8, 16);

  // This arithmetic can be expected to be exact, since all
  // values and results have exact representations.
  EXPECT_EQ(4 * a, b);
  EXPECT_EQ(a * 4, b);
  EXPECT_EQ(a / 0.25, b);
}

TEST(Geometry, DotProduct) {
  auto e1 = Vec3::e1();
  auto e2 = Vec3::e2();
  auto e3 = Vec3::e3();

  EXPECT_EQ(dot(e1, e2), 0);
  EXPECT_EQ(dot(e1, e3), 0);
  EXPECT_EQ(dot(e2, e3), 0);

  EXPECT_NEAR(dot(e1, e1), 1, 1e-12);
  EXPECT_NEAR(dot(e2, e2), 1, 1e-12);
  EXPECT_NEAR(dot(e3, e3), 1, 1e-12);

  EXPECT_NEAR(dot(2 * e1, 3 * e1), 2 * 3, 1e-12);
  EXPECT_NEAR(dot(e1 + e2, e2 + e3), 1, 1e-12);
}

TEST(Geometry, Length) {
  EXPECT_NEAR(length(Vec3(1, 0, 0)), 1, 1e-12);
  EXPECT_NEAR(length(Vec3(13, 0, 0)), 13, 1e-12);
  EXPECT_NEAR(length(Vec3(0, 3, 4)), 5, 1e-12);
}

TEST(Geometry, Map) {
  // Wrap in a lambda because of the "call-only" contract of standard
  // library functions.
  auto abs = [](double x) { return std::abs(x); };
  EXPECT_EQ(Vec3(1, 2, 3), Vec3(-1, 2, -3).map(abs));
}

TEST(Geometry, Normalized) {
  Vec3 a(1, -2, 3);
  Vec3 au = normalized(a);

  EXPECT_NEAR(length(au), 1, 1e-12);
  EXPECT_NEAR(dot(au, a), length(a), 1e-12);
}

TEST(Geometry, Cross) {
  // Create a messy but orthonormal triple of vectors.
  Vec3 a = normalized(Vec3(1, 2, 3));
  Vec3 b = Vec3(2, 1, -1);
  b = normalized(b - dot(a, b) * a);
  Vec3 c = cross(a, b);
  EXPECT_NEAR(length(c), 1, 1e-12);

  // length(X-Y) checks are a way to ask "are X and Y near?"
  EXPECT_NEAR(length(cross(b, c) - a), 0, 1e-12);
  EXPECT_NEAR(length(cross(c, a) - b), 0, 1e-12);

  // axb == -bxa
  EXPECT_NEAR(length(cross(b, c) + cross(c, b)), 0, 1e-12);
}

TEST(Geometry, Ray) {
  const auto e1 = Vec3::e1();
  const auto e2 = Vec3::e2();
  const auto e3 = Vec3::e3();

  // Test a bunch of points along the ray, they should all remain
  // (exactly) orthogonal to e3.
  EXPECT_EQ(Ray(e1, e2)(2.0), e1 + 2 * e2);
  for (double d = -5.0; d <= 10.0; d += 0.5) {
    EXPECT_EQ(dot(e3, Ray(e1, e2)(d)), 0.0);
  }
}

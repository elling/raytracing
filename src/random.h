#pragma once

#include <random>

#include "geometry.h"

// Wrappers for random number generation.

// Note: changed the naming convention from the book to rely on
// names instead of overload resolution to disambiguate.
inline double random_unit() {
  // A note about "static": this means that the variable's lifetime is
  // like a global, and it is initialized once during the run of the
  // program.
  static std::uniform_real_distribution<double> distribution(0.0, 1.0);
  static std::mt19937 generator;
  return distribution(generator);
}

inline double random_double(double mn, double mx) {
  return mn + (mx - mn) * random_unit();
}

inline Vec3 random_dir() {
  while (true) {
    Vec3 pos(random_double(-1, 1), random_double(-1, 1), random_double(-1, 1));
    if (dot(pos, pos) < 1) {
      return normalized(pos);
    }
  }
}

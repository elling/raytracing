#include "image.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <sstream>

std::uint8_t unit_to_byte(double x) {
  return (int)std::round(255 * std::clamp(x, 0.0, 1.0));
}

PpmImage::PpmImage(unsigned int width, unsigned int height)
    : width_(width), height_(height), data_(width * height * 3) {}

PpmImage::PpmImage(const nlohmann::json &image_json)
    : PpmImage(image_json["width"].get<unsigned int>(),
               image_json["height"].get<unsigned int>()) {}

void PpmImage::set(unsigned int x, unsigned int y, Vec3 color) {
  const auto offset = 3 * (x + width_ * y);
  data_[offset + 0] = unit_to_byte(color[0]);
  data_[offset + 1] = unit_to_byte(color[1]);
  data_[offset + 2] = unit_to_byte(color[2]);
}

std::string PpmImage::writeString() const {
  std::ostringstream oss;
  oss << "P3\n";
  oss << width_ << " " << height_ << "\n";
  oss << "255\n";
  for (unsigned int y = 0; y < height_; ++y) {
    for (unsigned int x = 0; x < width_; ++x) {
      const auto offset = 3 * (x + width_ * y);
      oss << (x ? " " : "") << (int)data_[offset + 0] << " "
          << (int)data_[offset + 1] << " " << (int)data_[offset + 2];
    }
    oss << "\n";
  }
  return oss.str();
}

void PpmImage::writeFile(const std::string &filename) const {
  std::ofstream out(filename);
  out << writeString();
  out.close();
}

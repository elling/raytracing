#include "camera.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(Camera, Simple) {
  const auto e1 = Vec3::e1();
  const auto e2 = Vec3::e2();
  const auto e3 = Vec3::e3();

  // Focus the camera 4 units away, looking in the x direction.
  //
  // Note: I _think_ this keeps the coordinates right-handed, but
  // I'm not 100% sure.
  Camera cam(e1, e1, e2, 4, e3, e2);

  // The rays all start at the original position,
  EXPECT_EQ(cam(0, 0)(0), e1);
  EXPECT_EQ(cam(1, 0)(0), e1);
  EXPECT_EQ(cam(0, 1)(0), e1);

  // Test that we normalized the length of the ray.
  EXPECT_NEAR(length(cam(0, 0)(1) - e1), 1, 1e-12);
  EXPECT_NEAR(length(cam(1, 0)(1) - e1), 1, 1e-12);
  EXPECT_NEAR(length(cam(0, 1)(1) - e1), 1, 1e-12);
  EXPECT_NEAR(length(cam(-1, -1)(1) - e1), 1, 1e-12);
}

TEST(Camera, ViaJson) {
  const auto e2 = Vec3::e2();

  const std::string cam_text = R"(
  {
    "position": [0, 1, 0],
    "direction": [0, 0, 1],
    "normal": [0, 1, 0],
    "focal_depth": 4,
    "view_size": [4, 3]
  }
  )";
  Camera cam(nlohmann::json::parse(cam_text));

  EXPECT_EQ(cam(0, 0)(0), e2);

  // Test that we normalized the length of the ray.
  EXPECT_NEAR(length(cam(0, 0)(1) - e2), 1, 1e-12);
  EXPECT_NEAR(length(cam(1, 0)(1) - e2), 1, 1e-12);
  EXPECT_NEAR(length(cam(0, 1)(1) - e2), 1, 1e-12);
  EXPECT_NEAR(length(cam(-1, -1)(1) - e2), 1, 1e-12);
}

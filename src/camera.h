#pragma once

#include "geometry.h"

#include "nlohmann/json.hpp"

// A "Camera" is basically a ray factory.  It can either be initialized
// via explicit vectors, or via a JSON spec (example in `scene.json`).
//
// This class explicitly produces rays with normalized direction vectors.
// There are cases where unnormalized is preferred (normalized screen depth,
// for example), but I suspect it'll be more useful for the parameter to
// correspond 1:1 with actual distance.
class Camera {
public:
  // Note: these vectors don't actually line up well to the JSON
  // spec, consider refactoring to make it the same.
  Camera(Vec3 position, Vec3 direction, Vec3 normal, double depth,
         Vec3 horizontal, Vec3 vertical);
  // Support initialization from JSON directly
  Camera(const nlohmann::json &cam);

  Ray operator()(double u, double v) const;

private:
  Vec3 position_;
  Vec3 direction_;
  Vec3 normal_;
  double depth_;
  Vec3 horizontal_;
  Vec3 vertical_;
};

#include "geometry.h"

Vec3 vec3_from_json(const nlohmann::json& obj) {
  return Vec3(obj[0], obj[1], obj[2]);
}

#pragma once

#include <cmath>
#include <limits>

// Common constants.
//
// Deviation from the book:  I prefer the more conventional `M_PI`,
// defined by the C library, over any custom name, just because it is more
// common to see in the wild.  I've also left out degrees_to_radians for now,
// I'll add it if/when useful, not before.  (User inputs are nice in degrees,
// but any internal math should be radians, radians, or radians.)

const double infinity = std::numeric_limits<double>::infinity();

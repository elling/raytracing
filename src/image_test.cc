#include "image.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(PpmImage, Empty) {
  EXPECT_THAT(PpmImage(0, 0).writeString(), "P3\n0 0\n255\n");
}

TEST(PpmImage, SmallEmpty) {
  EXPECT_THAT(PpmImage(3, 2).writeString(),
              "P3\n3 2\n255\n0 0 0 0 0 0 0 0 0\n0 0 0 0 0 0 0 0 0\n");
}

TEST(PpmImage, SmallRgb) {
  PpmImage image(3, 2);
  image.set(0, 0, Vec3(1, 0, 0));
  image.set(1, 0, Vec3(1, 1, 0));
  image.set(2, 1, Vec3(1, 0.5, 1));
  EXPECT_THAT(
      image.writeString(),
      "P3\n3 2\n255\n255 0 0 255 255 0 0 0 0\n0 0 0 0 0 0 255 128 255\n");
}

TEST(PpmImage, ClampRgb) {
  PpmImage image(1, 1);
  image.set(0, 0, Vec3(-1, 1, 2));
  EXPECT_THAT(image.writeString(), "P3\n1 1\n255\n0 255 255\n");
}

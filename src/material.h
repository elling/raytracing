#pragma once

#include "geometry.h"

#include "nlohmann/json.hpp"

#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

struct Scatter {
  Vec3 attenuation;
  Ray path;

  Scatter(const Vec3& a, const Ray& p) : attenuation(a), path(p) {}
};

struct HitRecord;

class Material {
public:
  virtual std::optional<Scatter> scatter(const Ray &ray,
                                         const HitRecord &hit) = 0;
  virtual std::string type() const { return "abstract"; }
};

class LambertianMaterial : public Material {
public:
  LambertianMaterial(const Vec3 &color) : albedo_(color) {}
  std::optional<Scatter> scatter(const Ray &ray, const HitRecord &hit);
  virtual std::string type() const { return "lambertian"; }

private:
  Vec3 albedo_;
};

class MetalMaterial : public Material {
  public:
    MetalMaterial(const Vec3 &color, double fuzz) : albedo_(color), fuzz_(fuzz) {}
  std::optional<Scatter> scatter(const Ray &ray, const HitRecord &hit);
  virtual std::string type() const { return "metal"; }

private:
  Vec3 albedo_;
  double fuzz_;
};

class DielectricMaterial : public Material {
  public:
    DielectricMaterial(double index) : index_(index) {}

  std::optional<Scatter> scatter(const Ray &ray, const HitRecord &hit);
  virtual std::string type() const { return "dielectric"; }

private:
  double index_;
};

// A ray-shape intersection.
//
// Deviation from the book: I go with strictly-outward normals.  This might
// be justified, since I'm planning on adding more (composite) geometry types,
// but it may also be a mistake.  Time will tell!
struct HitRecord {
  HitRecord(Vec3 position_, Vec3 normal_, double t_,
            std::shared_ptr<Material> mat_ = nullptr)
      : position(position_), normal(normal_), t(t_), material(mat_) {}

  HitRecord flipNorm() { return HitRecord(position, -normal, t); }

  Vec3 position;
  Vec3 normal;
  double t = 0;
  std::shared_ptr<Material> material;
};

using MatMap = std::unordered_map<std::string, std::shared_ptr<Material>>;

std::shared_ptr<Material> build_mat(const nlohmann::json &mat_json);

std::shared_ptr<Material> get_mat(const nlohmann::json &shape_json,
                                  const MatMap &materials);

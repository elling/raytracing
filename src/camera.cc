#include "camera.h"

Camera::Camera(Vec3 position, Vec3 direction, Vec3 normal, double depth,
               Vec3 horizontal, Vec3 vertical)
    : position_(position), direction_(direction), normal_(normal),
      depth_(depth), horizontal_(horizontal), vertical_(vertical) {}

Camera::Camera(const nlohmann::json &cam) {
  for (int j = 0; j < 3; ++j) {
    position_[j] = cam["position"][j].get<double>();
    direction_[j] = cam["direction"][j].get<double>();
    normal_[j] = cam["normal"][j].get<double>();
  }

  auto d = normalized(direction_);
  auto n = normalized(normal_);
  auto dxn = cross(d, n);

  depth_ = cam["focal_depth"].get<double>();

  horizontal_ = cam["view_size"][0].get<double>() * 0.5 * dxn;
  vertical_ = cam["view_size"][1].get<double>() * 0.5 * n;
}

Ray Camera::operator()(double u, double v) const {
  return Ray(position_,
             normalized(depth_ * direction_ + u * horizontal_ + v * vertical_));
}

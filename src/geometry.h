#pragma once

#include <cmath>
#include <ostream>
#include <utility>

#include "nlohmann/json.hpp"

// Implementation for 3d vectors.
//
// Deviation from book: most methods implemented out-of-line, a style
// that I prefer.  If you write against the public interface (assuming
// that the code is similarly-efficient), it's easier to refactor class
// internals.
//
// Follows rule-of-zero:
// [1]: https://en.cppreference.com/w/cpp/language/rule_of_three
class Vec3 {
public:
  Vec3() : data_{0, 0, 0} {}
  Vec3(double x, double y, double z) : data_{x, y, z} {}

  // Support for array-like access.  Left out x()/y()/... since
  // this is shorter, and better to keep the code uniform.
  double &operator[](unsigned int n) { return data_[n]; }
  const double &operator[](unsigned int n) const { return data_[n]; }

  // Not sure if this will be useful, but returns a new vector with a
  // function applied elementwise to the components.  Parameterized
  // over the function type so that it can be used efficiently with
  // lambdas.
  template <typename Fn> Vec3 map(Fn fn) const {
    return Vec3(fn(data_[0]), fn(data_[1]), fn(data_[2]));
  }

  // Another experiment --- support e_j notation?
  static Vec3 e1() { return Vec3(1, 0, 0); }
  static Vec3 e2() { return Vec3(0, 1, 0); }
  static Vec3 e3() { return Vec3(0, 0, 1); }

private:
  double data_[3];
};

// Note that this is true equality, and comes with the normal baggage re:
// floating point comparison (including the possibility of `v == v` being
// false, in the presence of NaNs).
inline bool operator==(const Vec3 &a, const Vec3 &b) {
  return a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
}

inline Vec3 operator+(const Vec3 &a, const Vec3 &b) {
  return Vec3(a[0] + b[0], a[1] + b[1], a[2] + b[2]);
}

inline Vec3 &operator+=(Vec3 &a, const Vec3 &b) {
  a[0] += b[0];
  a[1] += b[1];
  a[2] += b[2];
  return a;
}

inline Vec3 operator-(const Vec3 &a, const Vec3 &b) {
  return Vec3(a[0] - b[0], a[1] - b[1], a[2] - b[2]);
}

// Single argument, negation.
inline Vec3 operator-(const Vec3 &a) { return Vec3(-a[0], -a[1], -a[2]); }

// Support either ordering for scalar multiplication.
inline Vec3 operator*(double k, const Vec3 &a) {
  return Vec3(k * a[0], k * a[1], k * a[2]);
};
inline Vec3 operator*(const Vec3 &a, double k) {
  return Vec3(k * a[0], k * a[1], k * a[2]);
};

// Also support element-wise multiplication
inline Vec3 operator*(const Vec3 &a, const Vec3 &b) {
  return Vec3(a[0]*b[0], a[1]*b[1], a[2]*b[2]);
}

// Scalar division only makes sense in one order.
inline Vec3 operator/(const Vec3 &a, double k) {
  return Vec3(a[0] / k, a[1] / k, a[2] / k);
};

inline double dot(const Vec3 &a, const Vec3 &b) {
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

inline Vec3 cross(const Vec3 &a, const Vec3 &b) {
  return Vec3(a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2],
              a[0] * b[1] - a[1] * b[0]);
}

inline Vec3 reflect(const Vec3& a, const Vec3& b) {
  return a - 2 * dot(a, b) * b;
}

inline double length(const Vec3 &a) { return std::sqrt(dot(a, a)); }

inline Vec3 normalized(const Vec3 &a) { return a / length(a); }

inline std::ostream &operator<<(std::ostream &out, const Vec3 &a) {
  return out << "(" << a[0] << "," << a[1] << "," << a[2] << ")";
}

class Ray {
public:
  Ray(const Vec3 &origin, const Vec3 &direction)
      : origin_(origin), direction_(direction) {}

  const Vec3 &origin() const { return origin_; }
  const Vec3 &direction() const { return direction_; }

  // Since a ray is really a 1d parameterization of a Vec3, may as well use
  // operator() so that we can just write "ray(1.5)" instead of
  // "ray.at(1.5)".
  Vec3 operator()(double t) const { return origin_ + t * direction_; }

private:
  Vec3 origin_;
  Vec3 direction_;
};

Vec3 vec3_from_json(const nlohmann::json& obj);

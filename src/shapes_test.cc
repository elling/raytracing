#include "constants.h"
#include "shapes.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(Shapes, UnitSphere) {
  Sphere unit_sphere(Vec3(), 1);

  auto hit = unit_sphere.scan(Ray(Vec3(), Vec3::e1()), 0, infinity);
  ASSERT_TRUE(hit.has_value());
  EXPECT_NEAR(hit->t, 1.0, 1e-12);

  // Advancing the `t` to the previous hit should fail to find a second.
  hit = unit_sphere.scan(Ray(Vec3(), Vec3::e1()), hit->t, infinity);
  EXPECT_FALSE(hit.has_value());

  // And a ray that doesn't intersect at all will fail to find any hit.
  hit = unit_sphere.scan(Ray(Vec3(0, 0, 2), Vec3::e1()), -infinity, infinity);
  EXPECT_FALSE(hit.has_value());
}

TEST(Shapes, UnitCube) {
  Cube unit_cube(Vec3(), 0.5);

  auto hit = unit_cube.scan(Ray(Vec3(), Vec3::e1()), 0, infinity);
  ASSERT_TRUE(hit.has_value());
  EXPECT_NEAR(hit->t, 0.5, 1e-12);

  hit = unit_cube.scan(Ray(Vec3(), Vec3::e1()), -1, infinity);
  ASSERT_TRUE(hit.has_value());
  EXPECT_NEAR(hit->t, -0.5, 1e-12);

  hit = unit_cube.scan(Ray(Vec3(), Vec3::e1() + 2 * Vec3::e2()), 0, infinity);
  ASSERT_TRUE(hit.has_value());
  EXPECT_NEAR(hit->t, 0.25, 1e-12);
}

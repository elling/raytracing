#include "shapes.h"
#include <iostream>

void HittableList::add(std::shared_ptr<Hittable> hittable) {
  hittables_.push_back(hittable);
}

std::optional<HitRecord> HittableList::scan(const Ray &ray, double t_min,
                                            double t_max) const {
  // It might be slightly slower, but use an optional so that we can
  // avoid anything like "float max" or other nonsense.  In opt mode
  // the compiler is expected to make it pretty efficient.
  std::optional<HitRecord> min_rec;
  for (const auto &hittable : hittables_) {
    const auto rec = hittable->scan(ray, t_min, t_max);
    if (rec && (rec->t > t_min && rec->t <= t_max) &&
        (!min_rec || rec->t < min_rec->t)) {
      min_rec = rec;
    }
  }

  return min_rec;
}

bool HittableList::inside(const Vec3 &pos) const {
  for (const auto &hittable : hittables_) {
    if (hittable->inside(pos))
      return true;
  }
  return false;
}

Sphere::Sphere(const nlohmann::json &sphere_json, const MatMap &materials) {
  center_ = vec3_from_json(sphere_json["position"]);
  radius_ = sphere_json["radius"].get<double>();
  material_ = get_mat(sphere_json, materials);

  std::cerr << "Sphere Material " << (
      material_ ? material_->type() : "<nullptr>") << "\n";
}

std::optional<HitRecord> Sphere::scan(const Ray &ray, double t_min,
                                      double t_max) const {
  const auto p = ray.origin() - center_;
  const auto &q = ray.direction();

  // I differ from the book in that I keep the quadratic formula
  // in the traditional form, rather than factoring out the 1/2.
  //
  // The form in the book might save a FLOP or two, I'm not sure,
  // but I opted for the more familiar notation.
  //
  // || . || = 2-norm, T = transpose
  // ||p + tq|| = r^2
  // (p+tq)T(p+tq) = r^2
  // pTp + 2pTqt + qTqt^2 = r^2
  // qTq t^2 + 2 pTq t + pTp - r^2 = 0
  const double a = dot(q, q);
  const double b = 2 * dot(p, q);
  const double c = dot(p, p) - radius_ * radius_;

  const double discrim = b * b - 4 * a * c;
  if (discrim < 0) {
    return std::nullopt;
  }
  // Only spend the cycles on sqrt() if a solution exists.
  const double discrim_root = sqrt(discrim);

  double t = (-b - discrim_root) / (2 * a);
  if (t <= t_min) {
    t = (-b + discrim_root) / (2 * a);
  }

  if (t_min < t && t <= t_max) {
    const auto pos = ray(t);
    return HitRecord(pos, (pos - center_) / radius_, t, material_);
  }

  return std::nullopt;
}

bool Sphere::inside(const Vec3 &pos) const {
  const auto x = (pos - center_);
  return dot(x, x) < radius_ * radius_;
}

bool Cube::inside(const Vec3 &pos) const {
  const auto x = (pos - center_);
  return std::fabs(x[0]) < radius_ && std::fabs(x[1]) < radius_ &&
         std::fabs(x[2]) < radius_;
}

std::optional<HitRecord> best(const std::optional<HitRecord> a,
                              const std::optional<HitRecord> b) {
  if (!a)
    return b;
  if (!b)
    return a;

  return (a->t < b->t) ? a : b;
}

Cube::Cube(const nlohmann::json &sphere_json) {
  for (int j = 0; j < 3; ++j) {
    center_[j] = sphere_json["position"][j].get<double>();
  }
  radius_ = sphere_json["radius"].get<double>();
  // std::cout << "CUBE " << center_ << " " << radius_ << "\n";
}

std::optional<HitRecord> Cube::scan(const Ray &ray, double t_min,
                                    double t_max) const {
  auto x = scan_axis(ray, t_min, t_max, 0);
  auto y = scan_axis(ray, t_min, t_max, 1);
  auto z = scan_axis(ray, t_min, t_max, 2);

  return best(x, best(y, z));
}

std::optional<HitRecord> Cube::scan_axis(const Ray &ray, double t_min,
                                         double t_max, int axis) const {
  if (std::fabs(ray.direction()[axis]) < 1e-12)
    return std::nullopt;

  double tn =
      (center_[axis] - radius_ - ray.origin()[axis]) / ray.direction()[axis];
  double tp =
      (center_[axis] + radius_ - ray.origin()[axis]) / ray.direction()[axis];

  // std::cerr << tn << " " << tp << "\n";

  auto a1 = (axis + 1) % 3;
  auto a2 = (axis + 2) % 3;

  std::optional<HitRecord> hn;
  if (t_min < tn && tn <= t_max) {
    Vec3 pos = ray(tn);
    if (std::fabs(pos[a1] - center_[a1]) <= radius_ &&
        std::fabs(pos[a2] - center_[a2]) <= radius_) {
      Vec3 norm;
      norm[axis] = -1;
      hn = HitRecord(pos, norm, tn);
    }
  }

  std::optional<HitRecord> hp;
  if (t_min < tp && tp <= t_max) {
    Vec3 pos = ray(tp);
    if (std::fabs(pos[a1] - center_[a1]) <= radius_ &&
        std::fabs(pos[a2] - center_[a2]) <= radius_) {
      Vec3 norm;
      norm[axis] = 1;
      hp = HitRecord(pos, norm, tp);
    }
  }

  return best(hn, hp);
}

Difference::Difference(const nlohmann::json &diff_json, const MatMap &materials)
    : shape_(from_json(diff_json["shape"], materials)),
      negate_(from_json(diff_json["negate"], materials)) {}

std::optional<HitRecord> Difference::scan(const Ray &ray, double t_min,
                                          double t_max) const {
  // TODO(tim): Clean this up.  We'll iterate over intersections, until:
  //
  // (1) There's a positive-shape-hit that's not inside negate
  // (2) There's a negative-shape-hit that's inside shape
  // (3) t > t_max

  double t_last = -99999.0;
  while (t_min < t_max) {
    if (t_min == t_last) {
      std::cout << "t_min conflict: " << t_min << std::endl;
      exit(1);
    } else {
      t_last = t_min;
    }

    auto hit_s = shape_->scan(ray, t_min, t_max);
    // std::cerr << (bool) hit_s << hit_s->t << std::endl;

    auto hit_n = negate_->scan(ray, t_min, t_max);

    if (hit_s && (!hit_n || hit_s->t <= hit_n->t)) {
      // Iterate from hit_s
      if (t_min == hit_s->t) {
        std::cerr << "LOLWUT S\n";
        exit(1);
      }
      t_min = hit_s->t;
      if (!negate_->inside(hit_s->position))
        return hit_s;
    } else if (hit_n) {
      if (t_min == hit_n->t) {
        std::cerr << "LOLWUT N\n";
        exit(1);
      }
      // Iterate from hit_n
      t_min = hit_n->t;
      if (shape_->inside(hit_n->position)) {
        return {hit_n->flipNorm()};
      }
    } else {
      break;
    }
  }

  return std::nullopt;
}

std::shared_ptr<Hittable> from_json(const nlohmann::json &shape_json,
                                    const MatMap &materials) {
  if (shape_json["type"] == "sphere") {
    return std::make_shared<Sphere>(shape_json, materials);
  } else if (shape_json["type"] == "cube") {
    return std::make_shared<Cube>(shape_json);
  } else if (shape_json["type"] == "difference") {
    return std::make_shared<Difference>(shape_json, materials);
  } else {
    std::cerr << "UNMATCHED SHAPE: " << shape_json << "\n";
    return nullptr;
  }
}

bool Difference::inside(const Vec3 &pos) const {
  return shape_->inside(pos) && !negate_->inside(pos);
}

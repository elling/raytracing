#include "material.h"

#include "random.h"

#include <memory>
#include <iostream>

std::shared_ptr<Material> get_mat(const nlohmann::json &shape_json,
                                  const MatMap &materials) {

  const std::string name = shape_json.contains("material")
                               ? shape_json["material"].get<std::string>()
                               : "";
  auto it = materials.find(name);
  if (it == materials.end()) {
    return nullptr;
  }

  return it->second;
}

std::optional<Scatter> LambertianMaterial::scatter(const Ray &ray,
                                                   const HitRecord &hit) {
  auto scat_dir = hit.normal + random_dir();
  while (dot(scat_dir, scat_dir) < 1e-8) {
    scat_dir = hit.normal + random_dir();
  }

  return {Scatter(albedo_, Ray(hit.position, normalized(scat_dir)))};
}

std::optional<Scatter> MetalMaterial::scatter(const Ray &ray,
                                              const HitRecord &hit) {
  Vec3 reflected = reflect(ray.direction(), hit.normal);

  if (dot(reflected, hit.normal) > 0) {
    Vec3 frefl = reflected + fuzz_ * random_dir();
    while (dot(frefl, frefl) < 1e-8) {
      frefl = reflected + fuzz_ * random_dir();
    }

    return {Scatter(albedo_, Ray(hit.position, normalized(frefl)))};
  }
  return std::nullopt;
}

Vec3 refract(const Vec3& dir, const Vec3& norm, double ratio) {
  const double cos_theta = std::min(dot(-dir, norm), 1.0);
  auto r_out_perp = ratio * (dir + cos_theta * norm);
  auto r_out_parr = std::sqrt(std::abs(1.0 - length(r_out_perp))) * norm;
  return r_out_perp + r_out_parr;
}

std::optional<Scatter> DielectricMaterial::scatter(const Ray &ray,
                                              const HitRecord &hit) {
  Vec3 albedo(1, 1, 1);
  const double ratio =
    dot(ray.direction(), hit.normal) > 0 ?
    (1.0 / index_) : index_;

  const Vec3 refracted = refract(ray.direction(), hit.normal, ratio);
  return {Scatter(albedo, Ray(hit.position, normalized(refracted)))};
}



std::shared_ptr<Material> build_mat(const nlohmann::json &mat_json) {
  if (mat_json.contains("lambertian")) {
    return std::make_shared<LambertianMaterial>(vec3_from_json(mat_json["lambertian"]["albedo"]));
  }
  if (mat_json.contains("metal")) {
    return std::make_shared<MetalMaterial>(vec3_from_json(mat_json["metal"]["albedo"]), mat_json["metal"]["fuzz"].get<double>());
  }
  if (mat_json.contains("dielectric")) {
    return std::make_shared<DielectricMaterial>(mat_json["dielectric"]["index"].get<double>());
  }
  return nullptr;
}

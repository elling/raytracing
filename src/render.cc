#include "camera.h"
#include "constants.h"
#include "geometry.h"
#include "image.h"
#include "random.h"
#include "shapes.h"

#include "nlohmann/json.hpp"

#include <fstream>
#include <iostream>
#include <memory>

Vec3 raytrace(const HittableList &world, const Ray &ray, double u, double v,
              int depth) {

  if (depth <= 0) return Vec3(0, 0, 0);

  const auto hit_opt = world.scan(ray, 0, infinity);
  if (hit_opt) {

    // If we have a material, use it.
    if (hit_opt->material) {
      auto scat_opt = hit_opt->material->scatter(ray, *hit_opt);
      if (scat_opt) {
        return scat_opt->attenuation * raytrace(world, scat_opt->path, u, v, depth - 1);
      } else {
        return Vec3(0, 0, 0);
      }
    }

    auto p = hit_opt->position + hit_opt->normal + random_dir();

    return 0.5 * raytrace(world, Ray(hit_opt->position, p - hit_opt->position),
                          u, v, depth + 1);
    // return 0.5 * (hit_opt->normal + Vec3(1, 1, 1));
  } else {
    // Background color based on u,v only.
    return Vec3((1 + u) / 2, (1 + v) / 2, 1.0);
  }
}

int main(int argc, char **argv) {
  if (argc != 3) {
    std::cerr << "Usage: render INFILE_JSON OUTFILE_PPM\n";
    return 1;
  }

  std::ifstream in(argv[1]);
  nlohmann::json scene = nlohmann::json::parse(in);

  const int samples = scene["samples"].get<int>();
  Camera camera(scene["camera"]);
  PpmImage image(scene["image"]);

  MatMap materials;
  auto& json_mats = scene["materials"];
  for (const auto& [key, value] : json_mats.items()) {
    materials[key] = build_mat(value);
    std::cerr << "Loaded mat " << key << " : " << materials[key]->type() << "\n";
  }
  // Put in a dumb default
  //materials[""] = std::make_shared<LambertianMaterial>(Vec3(0, 0, 0));

  HittableList objects;
  for (const auto &object : scene["objects"]) {
    auto hittable = from_json(object, materials);
    if (hittable)
      objects.add(hittable);
    /*
    // TODO(tim): There should be a factory in the shapes
    // library to handle dispatch.
    if (object["type"] == "sphere") {
      objects.add(std::make_shared<Sphere>(object));
    } else if (object["type"] == "cube") {
      objects.add(std::make_shared<Cube>(object));
    }
    */
  }

  // The last-reported pct complete, used for reporting progress only.
  double prev_pct = 0;

  for (unsigned int y = 0; y < image.height(); ++y) {
    for (unsigned int x = 0; x < image.width(); ++x) {
      Vec3 color;
      for (int n = 0; n < samples; ++n) {
        const double u = image.u(x + random_double(-0.5, 0.5));
        const double v = image.v(y + random_double(-0.5, 0.5));
        const auto ray = camera(u, v);

        color += raytrace(objects, ray, u, v, 10);

        /*
        const auto hit_opt = objects.scan(ray, 0, infinity);

        if (hit_opt) {
          color += 0.5 * (hit_opt->normal + Vec3(1, 1, 1));
        } else {
          color += Vec3((1 + u) / 2, (1 + v) / 2, 1.0);
        }
        */
      }

      image.set(x, y,
                (color / samples).map([](double x) { return std::sqrt(x); }));
    }

    // Compute the done pct, and if we've moved enough since last time, report.
    const double done_pct = 100 * (y + 1.0) / image.height();
    if (done_pct - prev_pct >= 1) {
      std::cout << "Finished " << done_pct << "%\n";
      prev_pct = done_pct;
    }
  }

  image.writeFile(argv[2]);

  return 0;
}

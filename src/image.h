#pragma once

#include "geometry.h"

#include "nlohmann/json.hpp"

#include <cstdint>
#include <string>
#include <vector>

// I've decided to use [0, 1] to represent colors w/ Vec3, instead
// of [0, 255].  This maps the unit interval to the byte interval,
// with clamping.
std::uint8_t unit_to_byte(double x);

// Rather than stream the pixels out, I save the image to this
// object in memory, and then save it at the end.
//
// If nothing else, it makes unit testing easier.
class PpmImage {
public:
  PpmImage(unsigned int width, unsigned int height);
  PpmImage(const nlohmann::json &image_json);

  void set(unsigned int x, unsigned int y, Vec3 color);
  std::string writeString() const;
  void writeFile(const std::string &filename) const;

  unsigned int width() const { return width_; }
  unsigned int height() const { return height_; }

  // Transform pixel coordinates to normalized image coordinates.
  // Note that this also handles flipping the y-axis.
  //
  // Inputs are double so that fractional values can applied.
  //
  // [0, WIDTH-1], [0, HEIGHT-1] -> [-1, 1], [1, -1]
  double u(double x) const { return -1.0 + 2 * x / (width_ - 1); }
  double v(double y) const { return 1.0 - 2 * y / (height_ - 1); }

private:
  const unsigned int width_;
  const unsigned int height_;
  std::vector<std::uint8_t> data_;
};

#pragma once

#include "geometry.h"
#include "material.h"

#include "nlohmann/json.hpp"

#include <memory>
#include <optional>
#include <vector>

class Hittable {
public:
  virtual ~Hittable() = default;
  // Return the HitRecord for the smallest value of t, within (t_min, t_max),
  // such that ray(t) intersects the surface.  Return nullopt if there is no
  // such solution.
  //
  // Choice to make interval open is to allow feeding the hit record back in to
  // find the next hit, if desired.  I expect this may be useful later with
  // compound objects.
  //
  // TODO(tim): Consider adding a facing-specific version?  Else non-convex
  // objects will be invisible if the camera is inside of them (non-issue for
  // convex).
  virtual std::optional<HitRecord> scan(const Ray &ray, double t_min,
                                        double t_max) const = 0;

  // Test if a point is inside the shape.
  // TODO(tim): Consider making this a double in [-1, 1], so
  // that we have a fuzzy notion of inside-ness in case it
  // matters in some cases?
  virtual bool inside(const Vec3 &point) const = 0;
};

class HittableList : public Hittable {
public:
  virtual ~HittableList() = default;
  void add(std::shared_ptr<Hittable> hittable);
  std::optional<HitRecord> scan(const Ray &ray, double t_min,
                                double t_max) const override;
  bool inside(const Vec3 &pos) const override;

private:
  std::vector<std::shared_ptr<Hittable>> hittables_;
};

class Sphere : public Hittable {
public:
  Sphere(const Vec3 &center, double radius)
      : center_(center), radius_(radius) {}
  Sphere(const nlohmann::json &sphere_json, const MatMap &materials);

  ~Sphere() override = default;
  std::optional<HitRecord> scan(const Ray &ray, double t_min,
                                double t_max) const override;
  bool inside(const Vec3 &pos) const override;

private:
  Vec3 center_;
  double radius_;
  std::shared_ptr<Material> material_;
};

class Cube : public Hittable {
public:
  Cube(const Vec3 &center, double radius) : center_(center), radius_(radius) {}
  Cube(const nlohmann::json &cube_json);
  ~Cube() override = default;

  std::optional<HitRecord> scan(const Ray &ray, double t_min,
                                double t_max) const override;
  std::optional<HitRecord> scan_axis(const Ray &ray, double t_min, double t_max,
                                     int axis) const;

  bool inside(const Vec3 &pos) const override;

private:
  Vec3 center_;
  double radius_;
};

class Difference : public Hittable {
public:
  Difference(std::shared_ptr<Hittable> shape, std::shared_ptr<Hittable> negate)
      : shape_(shape), negate_(negate) {}
  Difference(const nlohmann::json &diff_json, const MatMap &materials);
  ~Difference() override = default;

  std::optional<HitRecord> scan(const Ray &ray, double t_min,
                                double t_max) const override;
  bool inside(const Vec3 &pos) const;

private:
  std::shared_ptr<Hittable> shape_;
  std::shared_ptr<Hittable> negate_;
};

std::shared_ptr<Hittable> from_json(const nlohmann::json &shape_json,
                                    const MatMap &materials);

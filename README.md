# Raytracing

This doesn't have any raytracing content yet, but it is planned
to be an implementation following the "Raytracing in One Weeked"
series.

## Running

Run hello world with the command:

```sh
bazel run //bazel_demo:hello
```

To run an actual render, you can do the following.  This uses the `bazel run`
wrapper, which requires explicit paths because it runs the command in a "sandbox".
This can be useful for development, but sometimes annoying.

This is the standard test render I run:

```sh
bazel run -c opt //src:render -- $(pwd)/src/scene.json $(pwd)/out.ppm
```

You can run the unit tests for the whole repository by
running:

```sh
bazel test //...
```

What's cool about this is that it runs tests based on depdendencies.
It only re-runs tests that could have been impacted by changes you
made to the code since last time.

## Files

* WORKSPACE is used for per-project Bazel configuration
  - This also imports our dependencies
* \*/BUILD is used to define compilation targets

Note that there are two hidden files that are also important:

* .bazelrc - Used to set the C++ standard to c++17
* .gitignore - Used to ignore Bazel's generated files

## TODO

* Add some helpers for JSON extraction (things like JSON -> Vec3)
* Reconcile JSON vs explicit ctor for Camera

## Other design nodes

I follow the `T& var` rather than `T &var` convention, which is technically wrong,
but more readable in common usage.  YMMV.
